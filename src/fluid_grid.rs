use graphics::{Context};
use opengl_graphics::GlGraphics;

use crate::{fluid_cell::*, mathops::*, neighbors::*, field::*};

const G: f64 = 9.81;

#[derive(Copy, Clone, Debug)]
pub enum RenderMode {
     Pressure,
     Density,
     DensityMaskedPressure,
}

impl RenderMode {
    pub fn next(&self) -> Self {
        match self {
            RenderMode::Pressure => RenderMode::Density,
            RenderMode::Density => RenderMode::DensityMaskedPressure,
            RenderMode::DensityMaskedPressure => RenderMode::Pressure,
        }
    }
}


pub struct FluidGrid {
    // Meta
    width: usize,
    height: usize,
    spacing: f64,
    // Computation
    relaxation: f64,
    iterations: u32,
    // Fields
    horiz: Field<f64>,
    vert : Field<f64>,
    cells: Field<FluidCell>,
    pub smoke : Field<f64>,
    // Properties
    density: f64,
}

impl FluidGrid {
    pub fn new(width: usize, height: usize, spacing: f64) -> Self {
        FluidGrid {
            width: width,
            height: height,
            spacing: spacing,
            relaxation: 1.9, // 1 < r < 2
            iterations: 100,
            // Fields
            horiz: Field::new(width + 1, height, spacing),
            vert : Field::new(width, height + 1, spacing),
            cells: Field::new(width, height, spacing),
            smoke: Field::new(width, height, spacing),
            density: 10.0
        }
    }

    pub fn render(&self, context: &Context, gl: &mut GlGraphics, mode: RenderMode) {
        use graphics::*;
        let (min_pressure, max_pressure) = self.pressure_range();
        let (min_smoke, max_smoke) = self.smoke.range();
        let mut ttl = 0.0;
        for s in self.smoke.iter() {
            ttl += s;
        }
        println!("Render mode: {:?}", mode);
        println!("pressure: {:15.4} )( {:.4}", min_pressure, max_pressure);
        println!("total smoke: {}", ttl);
        println!("----------------");

        use crate::colors::{VIRIDIS as CMAP, map_value, exalpha, BLACK};
        // Origin is at top left of screen
        for y in 0..self.height {
            for x in 0..self.width {
                let cell = self.cells.get(x, y).unwrap();
                let square = rectangle::square(x as f64 * self.spacing, y as f64 * self.spacing, self.spacing);
                let smoke_contents = *self.smoke.get(x, y).unwrap();
                let color = match cell.open {
                    true => match mode {
                        RenderMode::Pressure => exalpha(map_value(cell.pressure, min_pressure, max_pressure, &CMAP), 1.0),
                        RenderMode::Density => exalpha(map_value(smoke_contents, min_smoke, max_smoke, &CMAP), 1.0),
                        RenderMode::DensityMaskedPressure => {
                            if smoke_contents > 0.0 {
                                exalpha(map_value(cell.pressure, min_pressure, max_pressure, &CMAP),
                                        ((smoke_contents - min_smoke) / (max_smoke - min_smoke)) as f32)
                            } else {
                                BLACK
                            }
                        },
                    },
                    false => [0.1, 0.2, 0.7, 1.0],
                };
                rectangle(color, square, context.transform, gl);
            }
        }
    }

    fn pressure_range(&self) -> (f64, f64) {
        let mut min = f64::MAX;
        let mut max = 0.0;
        for cell in self.cells.iter() {
            if cell.pressure > max {
                max = cell.pressure;
            }
            if cell.pressure < min {
                min = cell.pressure;
            }
        }

        return (min, max);
    }

    pub fn update(&mut self, dt: f64) {
        // Velocity   (Update velocity field)
        // Projection (incompressability)
        // Advection  (move velocity field)

        self.update_gravity(dt);

        for cell in self.cells.iter_mut() {
            cell.pressure = 0.0;
        }
        for _ in 0..self.iterations {
            self.projection(dt);
        }

        self.advection(dt);
        self.advect_smoke(dt);
    }

    pub fn update_gravity(&mut self, dt: f64) {
        for y in 0..self.height {
            for x in 0..self.width {
                if self.cells.get(x, y).unwrap().open && self.cells.get(x, y - 1).and_then(|v| v.open.then(|| ())).is_some() {
                    *self.vert.get_mut(x, y).unwrap() += dt * G;
                }
            }
        }
    }

    pub fn projection(&mut self, dt: f64) {
        // Precompute value for pressure calculation
        let precomp = (self.density * self.spacing) / dt;

        for y in 0..self.height {
            for x in 0..self.width {
                if !self.cells.get(x, y).unwrap().open {
                    continue;
                }

                let divergence = self.divergence(x, y).unwrap();
                let neighbors = self.cells.neighbors(x, y);
                let neighbor_count = neighbors.count() as u32 as f64;

                let partial_d =  - divergence / neighbor_count * self.relaxation;

                let vtop = self.vtop(x, y);
                let stop = neighbors.t.s();
                let vleft = self.vleft(x, y);
                let sleft = neighbors.l.s();
                let vright = self.vright(x, y);
                let sright = neighbors.r.s();
                let vbot = self.vbot(x, y);
                let sbot = neighbors.b.s();
                self.set_top(  x, y, vtop   - (partial_d * stop  ));
                self.set_left( x, y, vleft  - (partial_d * sleft ));
                self.set_right(x, y, vright + (partial_d * sright));
                self.set_bot(  x, y, vbot   + (partial_d * sbot  ));

                // Handle pressure
                self.cells.get_mut(x, y).unwrap().pressure += partial_d * precomp;
            }
        }
    }

    pub fn advection(&mut self, dt: f64) {
        let mut new_horiz = Field::new(self.width + 1, self.height, self.spacing);
        let mut new_vert = Field::new(self.width, self.height + 1, self.spacing);

        for y in 0..self.height {
            for x in 0..self.width+1 {
                if !self.cells.get(x - 1, y).map_or(false, |c| c.open) || !self.cells.get(x, y).map_or(false, |c| c.open) {
                    continue;
                }

                let tl = match x {
                    0 => 0.0,
                    _ => *self.vert.get(x - 1, y).unwrap_or(&0.0),
                };
                let tr = *self.vert.get(x, y).unwrap_or(&0.0);
                let bl = match x {
                    0 => 0.0,
                    _ => *self.vert.get(x - 1, y + 1).unwrap_or(&0.0),
                };
                let br = *self.vert.get(x, y + 1).unwrap_or(&0.0);

                let tru_h = *self.horiz.get(x, y).unwrap();
                let avg_v = sum(&[tl, tr, bl, br]) / 4.0;

                let new_x = x as f64 - dt * tru_h / self.horiz.spacing;
                let new_y = y as f64 - dt * avg_v / self.vert.spacing;
                let sample = self.horiz.sample(new_x, new_y);
                new_horiz.set(x, y, sample);
            }
        }
        for y in 0..self.height+1 {
            for x in 0..self.width {
                if !self.cells.get(x, y - 1).map_or(false, |c| c.open) || !self.cells.get(x, y).map_or(false, |c| c.open) {
                    continue;
                }

                let tl = match y {
                    0 => 0.0,
                    _ => *self.horiz.get(x, y - 1).unwrap_or(&0.0),
                };
                let tr = match y {
                    0 => 0.0,
                    _ => *self.horiz.get(x + 1, y - 1).unwrap_or(&0.0),
                };
                let bl = *self.horiz.get(x, y).unwrap_or(&0.0);
                let br = *self.horiz.get(x + 1, y).unwrap_or(&0.0);

                let tru_v = *self.vert.get(x, y).unwrap();
                let avg_h = sum(&[tl, tr, bl, br]) / 4.0;

                let new_x = x as f64 - dt * avg_h / self.horiz.spacing;
                let new_y = y as f64 - dt * tru_v / self.vert.spacing;
                let sample = self.vert.sample(new_x, new_y);
                new_vert.set(x, y, sample);
            }
        }

        self.horiz = new_horiz;
        self.vert = new_vert;
    }

    fn advect_smoke(&mut self, dt: f64) {
        let mut new_smoke = Field::new(self.width, self.height, self.spacing);

        for y in 0..self.height {
            for x in 0..self.width {
                if self.cells.get(x, y).map_or(false, |c| c.open) {
                    // Compute average horizonal and vertical velocities (aka velocity at center of cell)
                    // We can straight up .unwrap() because we're accessing velocities while traversing cells
                    //     and not the velocity fields directly.
                    let v_horiz = (self.horiz.get(x, y).unwrap() + self.horiz.get(x + 1, y).unwrap()) * 0.5;
                    let v_vert = (self.vert.get(x, y).unwrap() + self.vert.get(x, y + 1).unwrap()) * 0.5;

                    let new_x = x as f64 - dt * v_horiz / self.horiz.spacing;
                    let new_y = y as f64 - dt * v_vert / self.vert.spacing;

                    let value = self.smoke.sample(new_x, new_y);
                    new_smoke.set(x, y, value);
                }
            }
        }

        self.smoke = new_smoke;
    }

    // Field helpers
    pub fn divergence(&self, x: usize, y: usize) -> Result<f64, &'static str> {
        if x > self.width || y > self.height {
            return Err("Invalid location to measure divergence");
        }

        let left  = self.vleft(x, y);
        let right = self.vright(x, y);
        let top   = self.vtop(x, y);
        let bot   = self.vbot(x, y);

        Ok(- top - left + right + bot)
    }

    pub fn vleft(&self, x: usize, y: usize) -> f64 {
        *self.horiz.get(x, y).unwrap()
    }
    pub fn set_left(&mut self, x: usize, y: usize, value: f64) {
        self.horiz.set(x, y, value);
    }
    pub fn vright(&self, x: usize, y: usize) -> f64 {
        *self.horiz.get(x + 1, y).unwrap()
    }
    pub fn set_right(&mut self, x: usize, y: usize, value: f64) {
        self.horiz.set(x + 1, y, value);
    }
    pub fn vtop(&self, x: usize, y: usize) -> f64 {
        *self.vert.get(x, y).unwrap()
    }
    pub fn set_top(&mut self, x: usize, y: usize, value: f64) {
        self.vert.set(x, y, value);
    }
    pub fn vbot(&self, x: usize, y: usize) -> f64 {
        *self.vert.get(x, y + 1).unwrap()
    }
    pub fn set_bot(&mut self, x: usize, y: usize, value: f64) {
        self.vert.set(x, y + 1, value);
    }

    pub fn set_open(&mut self, x: usize, y: usize, open: bool) {
        self.cells.get_mut(x, y).unwrap().open = open;
    }
}


#[cfg(test)]
mod test_grid {
    use super::FluidGrid;

    #[test]
    fn test_divergence_left_in() {
        let mut g = FluidGrid::new(1, 1, 5.0);
        g.set_left(0, 0, 1.0);

        let d = g.divergence(0, 0).unwrap();

        assert_eq!(d, -1.0);
    }

    #[test]
    fn test_divergence_all_in_out() {
        let mut g = FluidGrid::new(1, 1, 5.0);
        g.set_left(0, 0, 1.0);
        g.set_right(0, 0, 1.0);
        g.set_top(0, 0, 1.0);
        g.set_bot(0, 0, 1.0);

        let d = g.divergence(0, 0).unwrap();

        assert_eq!(d, 0.0);
    }

    #[test]
    fn test_divergence_all_in() {
        let mut g = FluidGrid::new(1, 1, 5.0);
        g.set_left(0, 0, 1.0);
        g.set_right(0, 0, -1.0);
        g.set_top(0, 0, 1.0);
        g.set_bot(0, 0, -1.0);

        let d = g.divergence(0, 0).unwrap();

        assert_eq!(d, -4.0);
    }

    #[test]
    fn test_divergence_all_out() {
        let mut g = FluidGrid::new(1, 1, 5.0);
        g.set_left(0, 0, -1.0);
        g.set_right(0, 0, 1.0);
        g.set_top(0, 0, -1.0);
        g.set_bot(0, 0, 1.0);

        let d = g.divergence(0, 0).unwrap();

        assert_eq!(d, 4.0);
    }
}
