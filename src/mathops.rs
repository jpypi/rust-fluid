use num_traits::{Zero};
use std::{cmp::min, ops::AddAssign};

pub fn sum<T: Copy + Zero + AddAssign>(x: &[T]) -> T {
    let mut total = Zero::zero();

    for i in 0..x.len() {
        total += x[i];
    }

    return total;
}

#[allow(dead_code)]
pub fn clamp_sub(x: usize, v: i32, default: i32) -> usize{
    min(x as i32 - v, default) as usize
}

#[cfg(test)]
mod test_mathops {
    use super::{clamp_sub, sum};

    #[test]
    fn test_clamp_sub() {
        let res = clamp_sub(2, -2, 1);
        assert_eq!(res, 1);
    }

    #[test]
    fn test_sum_u32() {
        let r: [u32; 4] = [1, 2, 3, 4];
        let res = sum(&r);
        assert_eq!(res, 10);
    }

    #[test]
    fn test_sum_f64() {
        let r: [f64; 4] = [1.5, 2.2, 3.1, 4.0];
        let res = sum(&r);
        assert_eq!(res, 10.8);
    }
}