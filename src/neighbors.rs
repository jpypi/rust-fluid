use crate::fluid_cell::FluidCell;

#[derive(Clone)]
pub struct Neighbors<T: Copy + Clone> {
    pub l:  Option<T>,
    pub r:  Option<T>,
    pub t:  Option<T>,
    pub b:  Option<T>,
    pub bl: Option<T>,
    pub br: Option<T>,
    pub tl: Option<T>,
    pub tr: Option<T>,
}

impl<T: Copy + Clone> Neighbors<T> {
    pub fn iter(&self) -> NeighborsIter<T> {
        NeighborsIter {
            neighbors: self.clone(),
            state: 0
        }
    }
}


pub trait CountableNeighbors {
    fn count(&self) -> usize;
}

impl CountableNeighbors for Neighbors<&FluidCell> {
    fn count(&self) -> usize {
        self.iter().map(|n| n.map_or(false, |c| c.open) as usize).reduce(|a, b| a + b).unwrap_or(4)
    }
}


pub struct NeighborsIter<T: Copy + Clone> {
    neighbors: Neighbors<T>,
    state: u8,
}

impl<T: Copy + Clone> Iterator for NeighborsIter<T> {
    type Item = Option<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let res = match self.state {
            0 => Some(self.neighbors.tl),
            1 => Some(self.neighbors.t),
            2 => Some(self.neighbors.tr),
            3 => Some(self.neighbors.r),
            4 => Some(self.neighbors.br),
            5 => Some(self.neighbors.b),
            6 => Some(self.neighbors.bl),
            7 => Some(self.neighbors.l),
            _ => None,
        };

        self.state += 1;

        return res;
    }
}


#[cfg(test)]
mod test_neighbors {
    use crate::fluid_cell::FluidCell;

    use super::{Neighbors, CountableNeighbors};


    #[test]
    fn test_neighbors_fluidcell_lrtb() {
        let n = Neighbors {
            l:  Some(&FluidCell{pressure: 0.0, open: true}),
            r:  Some(&FluidCell{pressure: 0.0, open: true}),
            t:  Some(&FluidCell{pressure: 0.0, open: true}),
            b:  Some(&FluidCell{pressure: 0.0, open: true}),
            bl: None,
            br: None,
            tl: None,
            tr: None,
        };

        assert_eq!(n.count(), 4);
    }

    #[test]
    fn test_neighbors_fluidcell_l() {
        let n = Neighbors {
            l:  Some(&FluidCell{pressure: 0.0, open: false}),
            r:  Some(&FluidCell{pressure: 0.0, open: true}),
            t:  Some(&FluidCell{pressure: 0.0, open: false}),
            b:  Some(&FluidCell{pressure: 0.0, open: false}),
            bl: None,
            br: None,
            tl: None,
            tr: None,
        };

        assert_eq!(n.count(), 1);
    }

    #[test]
    fn test_neighbors_fluidcell_r() {
        let n = Neighbors {
            l:  Some(&FluidCell{pressure: 0.0, open: false}),
            r:  Some(&FluidCell{pressure: 0.0, open: true}),
            t:  Some(&FluidCell{pressure: 0.0, open: false}),
            b:  Some(&FluidCell{pressure: 0.0, open: false}),
            bl: None,
            br: None,
            tl: None,
            tr: None,
        };

        assert_eq!(n.count(), 1);
    }

    #[test]
    fn test_neighbors_fluidcell_t() {
        let n = Neighbors {
            l:  Some(&FluidCell{pressure: 0.0, open: false}),
            r:  Some(&FluidCell{pressure: 0.0, open: false}),
            t:  Some(&FluidCell{pressure: 0.0, open: true}),
            b:  Some(&FluidCell{pressure: 0.0, open: false}),
            bl: None,
            br: None,
            tl: None,
            tr: None,
        };

        assert_eq!(n.count(), 1);
    }

    #[test]
    fn test_neighbors_fluidcell_b() {
        let n = Neighbors {
            l:  Some(&FluidCell{pressure: 0.0, open: false}),
            r:  Some(&FluidCell{pressure: 0.0, open: false}),
            t:  Some(&FluidCell{pressure: 0.0, open: false}),
            b:  Some(&FluidCell{pressure: 0.0, open: true}),
            bl: None,
            br: None,
            tl: None,
            tr: None,
        };

        assert_eq!(n.count(), 1);
    }

    #[test]
    fn test_neighbors_fluidcell_none() {
        let n = Neighbors {
            l:  Some(&FluidCell{pressure: 0.0, open: false}),
            r:  Some(&FluidCell{pressure: 0.0, open: false}),
            t:  Some(&FluidCell{pressure: 0.0, open: false}),
            b:  Some(&FluidCell{pressure: 0.0, open: false}),
            bl: None,
            br: None,
            tl: None,
            tr: None,
        };

        assert_eq!(n.count(), 0);
    }
}