extern crate glutin_window;
extern crate opengl_graphics;
extern crate piston;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::{EventLoop, ButtonEvent, Button, Key, ButtonState, MouseCursorEvent};
use piston::event_loop::{EventSettings, Events};
use piston::input::{RenderArgs, RenderEvent, UpdateArgs, UpdateEvent};
use piston::window::WindowSettings;
use rand::prelude::*;

mod fluid_grid;
mod fluid_cell;
mod mathops;
mod neighbors;
mod field;
mod colors;

use fluid_grid::*;


pub struct App {
    gl: GlGraphics,
    grid: FluidGrid,
    state: u32,
    on: bool,
    render_mode: RenderMode,
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        let grid = &self.grid;

        self.gl.draw(args.viewport(), |c, gl| {
            graphics::clear([0.0, 0.0, 0.0, 0.0], gl);

            grid.render(&c, gl, self.render_mode);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        let v = 300.0;
        if self.on {
            self.grid.set_left(20, 20, v);
            self.grid.set_left(20, 21, v);
            self.grid.set_left(20, 22, v);
            //self.grid.set_left(19, 20, v);
            //self.grid.set_left(19, 21, v);
            //self.grid.set_left(19, 22, v);

            self.grid.smoke.set(20, 20, 33.0);
            self.grid.smoke.set(20, 21, 33.0);
            self.grid.smoke.set(20, 22, 33.0);
        }

        self.grid.update(args.dt);
    }

    fn toggle_world(&mut self) {
        match self.state {
            0 => {
                // Reset other state
                self.grid.set_open(23, 19, true);
                self.grid.set_open(24, 19, true);
                self.grid.set_open(25, 19, true);
                self.grid.set_open(26, 19, true);
                self.grid.set_open(27, 19, true);
                self.grid.set_open(28, 20, true);
                self.grid.set_open(29, 21, true);
                self.grid.set_open(30, 22, true);
                self.grid.set_open(31, 23, true);
                self.grid.set_open(32, 24, true);

                // Set up new state
                self.grid.set_open(30, 16, false);
                self.grid.set_open(30, 17, false);
                self.grid.set_open(30, 18, false);

                self.grid.set_open(30, 20, false);
                self.grid.set_open(30, 21, false);
                self.grid.set_open(30, 22, false);

                self.grid.set_open(30, 24, false);
                self.grid.set_open(30, 25, false);
                self.grid.set_open(30, 26, false);

                self.state = 1;
            },
            1 => {
                // Reset other state
                self.grid.set_open(30, 16, true);
                self.grid.set_open(30, 17, true);
                self.grid.set_open(30, 18, true);
                self.grid.set_open(30, 20, true);
                self.grid.set_open(30, 21, true);
                self.grid.set_open(30, 22, true);
                self.grid.set_open(30, 24, true);
                self.grid.set_open(30, 25, true);
                self.grid.set_open(30, 26, true);

                // Set up new state
                self.grid.set_open(23, 19, false);
                self.grid.set_open(24, 19, false);
                self.grid.set_open(25, 19, false);
                self.grid.set_open(26, 19, false);

                self.grid.set_open(27, 19, false);
                self.grid.set_open(28, 20, false);
                self.grid.set_open(29, 21, false);
                self.grid.set_open(30, 22, false);
                self.grid.set_open(31, 23, false);
                self.grid.set_open(32, 24, false);

                // toggle
                self.state = 0;
            },
            _ => panic!(),
        }
    }
}

fn main() {
    let opengl = OpenGL::V3_2;

    let mut window: Window = WindowSettings::new("Fluid Sim", [500, 500])
        .graphics_api(opengl)
        .build()
        .unwrap();

    let mut app = App {
        gl: GlGraphics::new(opengl),
        grid: FluidGrid::new(100, 100, 5.0),
        state: 0,
        on: false,
        render_mode: RenderMode::Pressure,
    };
    //app.toggle_world();

    // Add some random fluid velocity
    /*
    let mut rng = rand::thread_rng();
    for y in 20..40 {
        for x in 20..40 {
            let value = rng.gen::<f64>() * 2.0 - 1.0;
            app.grid.set_left(x, y, 4.0);
            app.grid.set_top(x, y, value);
        }
    }
    */

    // Set some boundaries
    for y in [0, 50, 51, 99] {
        for x in 0..100 {
            app.grid.set_open(x, y, false);
        }
    }

    for x in [0, 99] {
        for y in 0..100 {
            app.grid.set_open(x, y, false);
        }
    }

    for y in [50, 51] {
        for x in 80..90 {
            app.grid.set_open(x, y, true);
        }
    }

    // Start up the main loop
    let ps = 30;
    let mut events = Events::new(EventSettings::new().max_fps(ps).ups(ps));

    let mut debounce = false;
    let mut drawing = false;
    while let Some(e) = events.next(&mut window) {
        if let Some(args) = e.render_args() {
            app.render(&args);
        }

        if let Some(args) = e.update_args() {
            app.update(&args);
        }

        if let Some(args) = e.mouse_cursor_args() {
            let x = args[0];
            let y = args[1];

            if drawing {
                let cell_xi = (x / 5.0) as usize;
                let cell_yi = (y / 5.0) as usize;

                app.grid.set_open(cell_xi, cell_yi, false);
            }
        }

        if let Some(args) = e.button_args() {
            match args.button {
                Button::Keyboard(Key::A) => {
                    if args.state == ButtonState::Press && !debounce{
                        debounce = true;
                        app.toggle_world();
                    } else if args.state == ButtonState::Release {
                        debounce = false;
                    }
                },
                Button::Keyboard(Key::Space) => {
                    if args.state == ButtonState::Release {
                        app.on = !app.on;
                    }
                },
                Button::Keyboard(Key::M) => {
                    if args.state == ButtonState::Release {
                        app.render_mode = app.render_mode.next();
                    }
                },
                Button::Mouse(piston::MouseButton::Left) => {
                    drawing = args.state == ButtonState::Press;
                }
                _ => (),
            };
        }
    }
}
