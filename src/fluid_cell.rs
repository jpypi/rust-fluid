#[derive(Copy, Clone)]
pub struct FluidCell {
    pub open:     bool,
    pub pressure: f64,
}

impl FluidCell {
    pub fn new() -> Self {
        FluidCell {
            open:     true,
            pressure: 0.0,
        }
    }

    pub fn s(&self) -> f64 {
        self.open as u32 as f64
    }
}

impl Default for FluidCell {
    fn default() -> Self {
        Self::new()
    }
}

pub trait OptionalCell {
    fn s(&self) -> f64;
}

impl OptionalCell for Option<&FluidCell> {
    fn s(&self) -> f64 {
        self.map_or(0.0, |c| c.s())
    }
}


/*
+---+---+
|   |   |
+---+---+
|   |   |
+---+---+
 */


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn s_false_to_f64() {
        let f = FluidCell{open: false, pressure: 0.0};
        assert_eq!(f.s(), 0.0);
    }

    #[test]
    fn s_true_to_f64() {
        let f = FluidCell{open: true, pressure: 0.0};
        assert_eq!(f.s(), 1.0);
    }

    #[test]
    fn test_clone() {
        let f = FluidCell{open: true, pressure: 3.297};
        let f2 = f.clone();

        assert_eq!(f2.open, f.open);
        assert_eq!(f2.pressure, f.pressure);
    }

    #[test]
    fn optional_cell_some() {
        let f = Some(&FluidCell{open: true, pressure: 0.0});
        assert_eq!(f.s(), 1.0);
    }

    #[test]
    fn optional_cell_none() {
        let f: Option<&FluidCell> = None;

        assert_eq!(f.s(), 0.0);
    }

}