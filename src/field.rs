use std::{ops::{Mul, Add, AddAssign}, fmt::Display};

use crate::neighbors::Neighbors;

pub struct Field<T: Copy> {
    data: Vec<T>,
    width: usize,
    height: usize,
    pub spacing: f64,
}

impl<T: Copy + Default> Field<T> {
    pub fn new(width: usize, height: usize, spacing: f64) -> Self {
        let capacity = width * height;
        let mut field = Vec::with_capacity(capacity);
        for _ in 0..capacity {
            field.push(T::default());
        }

        Field {
            data: field,
            width,
            height,
            spacing,
        }
    }

    pub fn get(&self, x: usize, y: usize) -> Option<&T> {
        if x >= self.width || y >= self.height {
            return None;
        }

        self.data.get(y * self.width + x)
    }

    pub fn get_mut(&mut self, x: usize, y: usize) -> Option<&mut T> {
        if x >= self.width || y >= self.height {
            return None;
        }

        self.data.get_mut(y * self.width + x)
    }

    pub fn set(&mut self, x: usize, y: usize, value: T) {
        if x >= self.width || y >= self.height {
            panic!("Point index out of bounds");
        }

        self.data[y * self.width + x] = value;
    }

    pub fn iter(&self) -> std::slice::Iter<'_, T> {
        self.data.iter()
    }

    pub fn iter_mut(&mut self) -> std::slice::IterMut<'_, T> {
        self.data.iter_mut()
    }

    pub fn neighbors(&self, x: usize, y: usize) -> Neighbors<&T> {
        Neighbors {
            l: match x {
                   0 => None,
                   _ => self.get(x - 1, y),
                },
            r: self.get(x + 1, y),
            t: match y {
                   0 => None,
                   _ => self.get(x, y - 1),
                },
            b: self.get(x, y + 1),
            bl: None,
            br: None,
            tl: None,
            tr: None,
        }
    }
}

pub trait NumField<T: Mul + Add> {
    fn sample(&self, x: f64, y: f64) -> T;
    fn add_at(&mut self, x: usize, y: usize, value: T);
    fn range(&self) -> (f64, f64);
}

impl NumField<f64> for Field<f64> {
    fn sample(&self, x: f64, y: f64) -> f64 {
        let xi = x.floor() as usize;
        let yi = y.floor() as usize;

        let dx = x - xi as f64;
        let dy = y - yi as f64;

        let d_right = dx / 1.0f64;
        let d_bott  = dy / 1.0f64;
        let d_left  = 1.0f64 - d_right;
        let d_top   = 1.0f64 - d_bott;

        if xi.checked_add(1).is_none() || yi.checked_add(1).is_none() {
            return 0.0;
        }

        d_left  * d_top  * self.get(xi    , yi    ).unwrap_or(&0.0) +
        d_right * d_top  * self.get(xi + 1, yi    ).unwrap_or(&0.0) +
        d_left  * d_bott * self.get(xi    , yi + 1).unwrap_or(&0.0) +
        d_right * d_bott * self.get(xi + 1, yi + 1).unwrap_or(&0.0)
    }

    fn add_at(&mut self, x: usize, y: usize, value: f64) {
        self.data[y * self.width + x] += value;
    }

    fn range(&self) -> (f64, f64) {
        let mut min = f64::MAX;
        let mut max = 0.0;
        for cell in self.iter() {
            if *cell > max {
                max = *cell;
            }
            if *cell < min {
                min = *cell;
            }
        }

        return (min, max);
    }
}

impl<T: Copy + AddAssign> AddAssign<T> for Field<T> {
    fn add_assign(&mut self, rhs: T) {
        for i in 0..self.data.len() {
            self.data[i] += rhs;
        }
    }
}

impl<T: Copy + Display> Display for Field<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                let v = self.data[y * self.width + x];
                v.fmt(f)?;
                if let Some(w) = f.width() {
                    write!(f, "{}", " ".repeat(w))?;
                } else {
                    write!(f, " ")?;
                }
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_samplefield() {
        let mut f: Field<f64> = Field::new(2, 2, 5.0);

        f.set(0, 0, 1.0);
        f.set(0, 1, 1.0);
        f.set(1, 0, 1.0);
        f.set(1, 1, 1.0);

        assert!(nearly_equal(f.sample(0.1, 0.1), 1.0));
    }

    #[test]
    fn test_samplefield_xgrad() {
        let mut f: Field<f64> = Field::new(2, 2, 5.0);
        f.set(0, 0, 2.0);
        f.set(0, 1, 2.0);
        f.set(1, 0, 1.0);
        f.set(1, 1, 1.0);

        let s = f.sample(0.1, 0.1);
        assert!(nearly_equal(s, 1.9), "{} == 1.9", s);
        let s = f.sample(0.1, 0.9);
        assert!(nearly_equal(s, 1.9), "{} == 1.9", s);
    }

    #[test]
    fn test_samplefield_ygrad() {
        let mut f: Field<f64> = Field::new(2, 2, 5.0);
        f.set(0, 0, 2.0);
        f.set(1, 0, 2.0);
        f.set(0, 1, 1.0);
        f.set(1, 1, 1.0);

        let s = f.sample(0.1, 0.1);
        assert!(nearly_equal(s, 1.9), "{} == 1.9", s);
        let s = f.sample(0.9, 0.1);
        assert!(nearly_equal(s, 1.9), "{} == 1.9", s);
    }

    #[test]
    fn test_iter() {
        use std::collections::HashSet;

        let mut valid = HashSet::new();
        valid.insert(&1);
        valid.insert(&2);
        valid.insert(&3);
        valid.insert(&4);

        let mut f: Field<u32> = Field::new(2, 2, 5.0);
        f.set(0, 0, 1);
        f.set(1, 0, 2);
        f.set(0, 1, 3);
        f.set(1, 1, 4);

        let mut h = HashSet::new();
        for i in f.iter() {
            h.insert(i);
        }

        assert_eq!(h, valid);
    }

    #[test]
    fn test_neighbors () {
        let mut f: Field<u32> = Field::new(3, 3, 5.0);
        f.set(0, 0, 1);
        f.set(1, 0, 2);
        f.set(2, 0, 3);

        f.set(0, 1, 4);
        f.set(1, 1, 5);
        f.set(2, 1, 6);

        f.set(0, 2, 7);
        f.set(1, 2, 8);
        f.set(2, 2, 9);


        let n_0_0 = f.neighbors(0, 0);
        assert_eq!(n_0_0.l, None);
        assert_eq!(n_0_0.t, None);
        assert_eq!(n_0_0.r, Some(&2));
        assert_eq!(n_0_0.b, Some(&4));

        let n_1_1 = f.neighbors(1, 1);
        assert_eq!(n_1_1.l, Some(&4));
        assert_eq!(n_1_1.t, Some(&2));
        assert_eq!(n_1_1.r, Some(&6));
        assert_eq!(n_1_1.b, Some(&8));

        let n_2_2 = f.neighbors(2, 2);
        assert_eq!(n_2_2.l, Some(&8));
        assert_eq!(n_2_2.t, Some(&6));
        assert_eq!(n_2_2.r, None);
        assert_eq!(n_2_2.b, None);
    }

    #[test]
    fn range() {
        let mut f: Field<f64> = Field::new(2, 2, 5.0);
        f.set(0, 0,   0.0);
        f.set(1, 0, -20.0);
        f.set(0, 1,  -3.0);
        f.set(1, 1,   4.0);

        let (min, max) = f.range();
        assert_eq!(min, -20.0);
        assert_eq!(max,   4.0);
    }

    #[test]
    fn add_at() {
        let mut f: Field<f64> = Field::new(2, 2, 5.0);
        f.set(0, 0,   0.0);
        f.set(1, 0, -20.0);
        f.set(0, 1,  -3.0);
        f.set(1, 1,   4.0);

        f.add_at(1, 0, 20.0);

        assert_eq!(*f.get(1, 0).unwrap(), 0.0);
    }

    #[test]
    fn get_out_of_bounds() {
        let f: Field<f64> = Field::new(2, 2, 5.0);

        let res_x = f.get(3, 1);
        let res_y = f.get(0, 3);

        assert_eq!(res_x, None);
        assert_eq!(res_y, None);
    }

    fn nearly_equal(a: f64, b: f64) -> bool {
        let abs_a = a.abs();
        let abs_b = b.abs();
        let diff = (a - b).abs();

        if a == b { // Handle infinities.
            true
        } else if a == 0.0 || b == 0.0 || diff < f64::MIN_POSITIVE {
            // One of a or b is zero (or both are extremely close to it,) use absolute error.
            diff < (f64::EPSILON * f64::MIN_POSITIVE)
        } else { // Use relative error.
            (diff / f64::min(abs_a + abs_b, f64::MAX)) < f64::EPSILON
        }
    }
}