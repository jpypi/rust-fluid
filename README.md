# Eulerian Fluid Simulation

Eulerian fluid simulation with semi-lagrangian advection.

# Usage

key      |functionality
---------|-------------
spacebar |toggle fluid flow on/off
m        |switch render mode (pressure, density, density masked pressure)
a        |add/toggle between flow redirection wall vs. double slit wall
mouse    |click: set cell.open to false (aka create a wall)

# Thanks
https://matthias-research.github.io/pages/tenMinutePhysics/index.html