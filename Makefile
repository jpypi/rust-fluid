# https://doc.rust-lang.org/rustc/instrument-coverage.html

setup:
	rustup component add llvm-tools
	cargo install rustfilt cargo-binutils cargo2junit

rust-fluid.profdata testoutput.json:
	RUSTFLAGS="-C instrument-coverage" cargo test --tests --message-format=json -- -Z unstable-options --format=json > testoutput.json
	rust-profdata merge -sparse default*.profraw -o rust-fluid.profdata

test-binary-name.txt: testoutput.json
	cat $< | jq -r "select(.profile.test == true) | .filenames[]" > $@

junit-test-report.xml: testoutput.json
	cat $< | cargo2junit > $@

TEST_IGNORE='/.cargo/registry|/cargo/registry/|library/std/src/thread/local.rs|/target/debug/build/'

lcov-rust-fluid.dat: rust-fluid.profdata test-binary-name.txt
	rust-cov export --color --ignore-filename-regex=$(TEST_IGNORE) --instr-profile=$< --object $$(cat test-binary-name.txt) --Xdemangler=rustfilt --format=lcov > $@

covreport: rust-fluid.profdata test-binary-name.txt
	rust-cov report --use-color --ignore-filename-regex=$(TEST_IGNORE) --instr-profile=$< --object $$(cat test-binary-name.txt)

covshow: rust-fluid.profdata test-binary-name.txt
	rust-cov show --use-color --ignore-filename-regex=$(TEST_IGNORE) --instr-profile=$< --object $$(cat test-binary-name.txt) --show-instantiations --show-line-counts-or-regions --line-coverage-lt=100 --Xdemangler=rustfilt

clean:
	rm -f *.profraw
	rm -f rust-fluid.profdata testoutput.json
	rm -f test-binary-name.txt
	rm -f junit-test-report.xml
	rm -f lcov-rust-fluid.dat

.PHONY: covreport covshow clean setup